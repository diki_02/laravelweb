$("#add_barang").click(function(){
    $("#modal_add").modal('show');
});

$(".edit_btn").click(function(){
    var id = $(this).val();

    var form = new FormData();

form.append("id", id);

var settings = {
  "async": false,
  "crossDomain": true,
  "url": "/barang/edit",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",

    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

var hasil = $.ajax(settings).done(function(response){}).responseText;
var json = JSON.parse(hasil);
var id_barang = document.getElementById("id_barang");
var nama_barang = document.getElementById("nama_barang");
var harga_barang = document.getElementById("harga_barang");

id_barang.value = json.id_barang;
nama_barang.value = json.nama_barang;
harga_barang.value = json.harga_barang;
console.log(json);
$("#modal_edit").modal("show");

});
$(".delete_btn").click(function(){
    var id = $(this).val();

    var form = new FormData();

form.append("id", id);

var settings = {
  "async": true,
  "crossDomain": true,
  "url": "/barang/delete",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",

    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

var hasil = $.ajax(settings).done(function(response){});
location.reload();
});
$("#impor").click(function(){
    $("#modal_impor").modal("show");
});
$(document).ready(function(){
    $("#add_barang_form").validate({
        nama_barang:{
            minlength:3,
            required:true
        },
        harga_barang:{
            minlength:3,
            required:true
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element.text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
        }
    });
});
