<?php

namespace App\Exports;

use App\Barang;
use Maatwebsite\Excel\Concerns\FromCollection;

class BarangExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        $barang = Barang::all();
        $barang_array[] = array();
        $barang_array[0] = array(
            'id'=>'ID Barang',
            'nama_barang'=>'Nama Barang',
            'harga_barang'=>'Harga Barang');
        $i = 1;
        foreach($barang as $row){
            $barang_array[$i] = array(
                'id'=>$row->id_barang,
                'nama_barang'=> $row->nama_barang,
                'harga_barang'=> $row->harga_barang);
            $i++;
        }
        $hasil = (object) $barang_array;
        return collect($hasil);
    }
}
