<?php

namespace App\Imports;
use App\Barang;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BarangImport implements ToModel,WithHeadingRow
{
    /**
    * @param Collection $collection
    */

    public function model(array $row)
    {
        return new Barang([
            'nama_barang'=>$row['nama_barang'],
            'harga_barang'=>$row['harga_barang'],
            'stok'=>$row['stok']
        ]);
    }
}
