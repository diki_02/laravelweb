<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = "model_news";
    protected $fillable = ['id_barang','nama_barang','harga_barang','stok'];
    public $incrementing = false;
    public function dbarang()
    {
        return $this->HasOne('App\DBarang','id_barang','id_barang');
    }
}
