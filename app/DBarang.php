<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DBarang extends Model
{
    protected $table = 'detail_transaksi';
    protected $fillable = ['id_transaksi','id_barang','jumlah'];
    public function barang()
    {
        return $this->belongsTo('App\Barang','id_barang','id_barang');
    }
}
