<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi2 extends Model
{
    protected $table = 'transaksi';
    protected $fillable = ['id_transaksi','tgl_transaksi','status'];
    public $incrementing = false;
}
