<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Barang;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Exports\BarangExport;
use App\Imports\BarangImport;
use App\Mail\NewMail;
use PDF;
use Mail;


class Baru extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        $model = Barang::paginate(10);
        return view('barang/barang',['data'=>$model]);

    }
    public function tambah()
    {

        return view('barang/tambah');
    }
    public function store(Request $request)
    {

        $this->validate($request,[
            'nama_barang'=>'required',
            'harga_barang'=>'required',
            'stok'=>'required'

        ]);
        // DB::table('model_news')->insert([
        //     'nama_barang'=> $request->nama_barang,
        //     'harga_barang'=> $request->harga_barang]);
        $barang = new Barang();
        $barang->nama_barang = $request->nama_barang;
        $barang->harga_barang = $request->harga_barang;
        $barang->stok = $request->stok;
        $barang->save();
        return redirect('/barang');
    }
    public function edit($id)
    {


        $barang  =  Barang::where('id_barang',$id)->get();
        return view('barang/edit',['barang'=>$barang]);
    }
    public function update(Request $request)
    {

        $this->validate($request,[
            'id'=>'required',
            'nama_barang'=>'required',
            'harga_barang'=>'required',
            'stok'=>'required'

        ]);
        $barang  =  Barang::where('id_barang',$request->id)->update([
            'nama_barang'=>$request->nama_barang,
            'harga_barang'=>$request->harga_barang,
            'stok'=>$request->stok]);
        // $barang->nama_barang = $request->nama_barang;
        // $barang->harga_barang = $request->harga_barang;
        // $barang->save();
        return redirect('/barang');
    }
    public function delete($id)
    {

        $barang  =  Barang::find($id);
        $barang->delete();
        redirect('/barang');
    }
    public function a()
    {
        return view('master');
    }
    public function export()
    {
         //$session = Session::flash('sukses','Data Berhasil di Expor');
         return Excel::download(new BarangExport,'barang.xlsx');
    }
    public function export_pdf()
    {
        $barang = Barang::all();
        $pdf = PDF::loadview('barang/barangpdf',['data'=>$barang]);
        return $pdf->stream('laporan_barang.pdf');
    }
    public function search(Request $request)
    {
        $cari = $request->cari;


        $hasil = Barang::where('nama_barang','like','%'.$cari.'%')->paginate();
        return view('barang/barang',['data'=>$hasil]);

    }
    public function import(Request $request)
    {
        // $this->validate($request,[
        //     'file'=>'required'
        // ]);

        $file = $request->file('file');
        //$hasil = Excel::load($file)->get();
        // if($hasil->count() > 0){
        //     foreach($hasil->toArray as $key=>$value){
        //         foreach($value as $row){
        //             $insert_data = array(
        //                 'nama_barang'=> $row['nama_barang'],
        //                 'harga_barang' => $row['harga_barang'],
        //                 'stok' => $row['stok']
        //             );
        //         }
        //     }
        //     if(!empty($insert_data)){
        //         DB::table('model_news')->insert($insert_data);
        //     }
        // }
        //dd($hasil);
        $filename = rand().$file->getClientOriginalName();
        $file->move('file_excel',$filename);
        Excel::import(new BarangImport,public_path('/file_excel/'.$filename));
        //Session::flash('sukses','Data Berhasil Di Impor');
        return redirect('/barang');

    }
    public function kirim()
    {
        for($i = 0;$i<10;$i++){
        Mail::to('pasalek738@cmailing.com')->send(new NewMail());
        }
        return "Email Terkirim";
    }

}

