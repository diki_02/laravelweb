<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Barang;
use App\Transaksi2;
use App\DBarang;
use Session;

class Transaksi extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $hasil = Transaksi2::orderBy('id_transaksi')->paginate(10);
        return view('transaksi/transaksi_list',['hasil'=>$hasil]);
    }
    public function add()
    {

        $barang  = Barang::all();

        $id_transaksi = date('Ymd').'_1';
        $transaksi = Transaksi2::all()->count();
        if($transaksi == 0){
            $transaksi2 = new Transaksi2();
            $transaksi2->id_transaksi = $id_transaksi;
            $transaksi2->tgl_transaksi = date('Y-m-d');
            $transaksi2->status = '1';
            $transaksi2->save();
            $hasil = Transaksi2::orderBy('id_transaksi','desc')->first();
        } else if($transaksi > 0){
            $transaksi3 = Transaksi2::where('status','1');

            if($transaksi3->count() > 0){
                $transaksi4 = Transaksi2::where('id_transaksi','20200211_1');
                $transaksi5 = Transaksi2::orderBy('id_transaksi','desc')->first();
                $hasil = $transaksi5;
            } else if($transaksi3->count() == 0) {
                $transaksi5 = Transaksi2::orderBy('id_transaksi','desc');
                $id_transaksi3 =  $transaksi5->first()->id_transaksi;
                $tgl_transaksi4 = substr($id_transaksi3,0,8);
                if($tgl_transaksi4 == date('Ymd')){
                    $no = str_replace(date('Ymd').'_',"",$id_transaksi3);
                    $no2 = $no + 1;
                    $id_transaksi5 = date('Ymd').'_'.$no2;
                    $hasil_id = $id_transaksi5;
                } else if( $tgl_transaksi4 != date('Ymd')){
                    $id_transaksi5 = date('Ymd').'_1';
                    $hasil_id = $id_transaksi5;
                }
                $transaksi6 = new Transaksi2();
                $transaksi6->id_transaksi = $hasil_id;
                $transaksi6->tgl_transaksi = date('Y-m-d');
                $transaksi6->status = '1';
                $transaksi6->save();
            }
            $hasil = Transaksi2::orderBy('id_transaksi','desc')->first();
        }
        $id_transaksi7 = Transaksi2::where('status','1')->first()->id_transaksi;
        //  $dbarang = DBarang::join('model_news',function ($qry) {
                // $qry->On('detail_transaksi.id_barang', '=', 'model_news.id')})->get();
            //  ->On('detail_transaksi.id_barang', '=', 'model_news.id')
            //  ->first();

        //  } 'detail_transaksi.id_barang','model_news.id')->get();
        //  $dbarang = DBarang::barang()->get();
        $dbarang = DBarang::join('model_news', function ($qry) use ($id_transaksi7) {
            $qry->on('detail_transaksi.id_barang', '=', 'model_news.id_barang')
                ->where('id_transaksi', $id_transaksi7)
                ;
        })->get();
        $dbarang2[] = array();
        $i = 0;
        $total_bayar = 0;
        // foreach($dbarang as $row){
        //     $barang4 = $this->cari_barang($row->id_barang);
        //     $total_harga = $barang4->harga_barang * $row->jumlah;
        //     $total_bayar += $total_harga;
        //     $dbarang2[$i] = array(
        //         'id_barang'=>$row->id_barang,
        //         'nama_barang'=>$barang4->nama_barang,
        //         'harga_barang'=>number_format($barang4->harga_barang),
        //         'jumlah'=>$row->jumlah,
        //         'total_harga' => number_format($total_harga));
        //         $i++;

        // }

        $data = array(
            'barang' => $barang,
            'transaksi' => $hasil,
            'dbarang'=>$dbarang,
            'total_bayar' => $total_bayar
        );
        // $a = "20200212_3";
        // $b = substr($a,0,8);
        //var_dump($b);

        //var_dump($dbarang);
        return view('transaksi/transaksi',$data);
    }
    public function add_barang($id)
    {
        $barang = Barang::where('id_barang',$id)->first();
        return view('transaksi/add_barang',['barang'=>$barang]);
    }
    public function beli_barang(Request $request)
    {

        $this->validate($request,[
            'id_barang'=>'required',
            'jumlah_barang'=>'required',

        ]);
        $stok = Barang::where('id_barang',$request->id_barang)->first()->stok;
        if($request->jumlah_barang <= $stok){
        $bayar = new DBarang();
        $id_transaksi = Transaksi2::where('status','1')->first()->id_transaksi;
        $hasil_stok = $stok - $request->jumlah_barang;
        $kurangin_stok = Barang::where('id_barang',$request->id_barang)->update([
            'stok'=> $hasil_stok,

        ]);
        $bayar->id_transaksi = $id_transaksi;
        $bayar->id_barang = $request->id_barang;
        $bayar->jumlah = $request->jumlah_barang;
        $bayar->save();
        return redirect('/transaksi/add');
        $a = $request->jumlah_barang;
        //var_dump($id_transaksi);
        // dd($hasil_stok);
        } else if($request->jumlah_barang > $stok){
            $message = "Stok tinggal ".$stok;
            Session::flash('gagal',$message);
            $url = "/transaksi/add_barang/".$request->id_barang;

           return redirect($url);
           //dd($url);
        //    $a = $stok;
        }
        // $a = array(
        //     'stok'=>$stok,
        //     'jumlah_beli'=>$request->jumlah_barang
        // );
        //dd($a);
    }
    public function cari_barang($id)
    {
        $barang = Barang::where('id_barang',$id)->first();
        return $barang;
    }
    public function bayar(Request $request)
    {
        $this->validate($request,[
            'total_bayar'=>'required',
            'jumlah_bayar'=> 'required',
            'kembalian'=>'required'
        ]);
        $transaksi = Transaksi2::where('status','1')->first();
        $dbarang = DBarang::where('id_transaksi',$transaksi->id_transaksi)->get();
        $dbarang2[] = array();
        $i = 0;
        $total_bayar = 0;
        foreach($dbarang as $row){
            $barang4 = $this->cari_barang($row->id_barang);
            $total_harga = $barang4->harga_barang * $row->jumlah;
            $dbarang2[$i] = array(
                'id_barang'=>$row->id_barang,
                'nama_barang'=>$barang4->nama_barang,
                'harga_barang'=>number_format($barang4->harga_barang),
                'jumlah'=>$row->jumlah,
                'total_harga' => number_format($total_harga));
                $i++;
                $total_bayar += $total_harga;
        }
        $transaksi2 = Transaksi2::where('status','1')
                                ->update(
                                    ['total_bayar'=>$request->total_bayar,
                                    'jumlah_bayar'=>$request->jumlah_bayar,
                                    'kembalian'=>$request->kembalian,
                                    'status'=>'2'
                                    ]);

        $data = array(
            'dbarang'=>$dbarang2,
            'total_bayar'=>$request->total_bayar,
            'transaksi'=>$transaksi,
            'jumlah_bayar'=>$request->jumlah_bayar,
            'kembalian' => $request->kembalian

        );
        return view('transaksi/struk',$data);


    }
}
