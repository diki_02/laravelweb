<?php

namespace App\Http\Controllers;
use App\Images;

use Illuminate\Http\Request;

class Image extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $hasil = Images::get();
        return view('gambar/gambar',['hasil'=>$hasil]);
    }
    public function insert()
    {
        return view('gambar/tambah');
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'gambar_'=>'required',
            'deskripsi' => 'required',
            ]);
        $file = $request->file('gambar_');


        $filename = time()."_".$file->getClientOriginalName();
        $lokasi = 'image';
        $file->move($lokasi,$filename);

        $image = new Images();
        $image->filename = $filename;
        $image->keterangan = $request->deskripsi;
        $image->save();
        return redirect('/gambar');


    }

}
