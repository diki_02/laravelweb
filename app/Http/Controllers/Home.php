<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi2;
use App\DBarang;
use App\Barang;

class Home extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $total_penjualan = Transaksi2::where('status','2')->sum('total_bayar');
        $barang_terjual = DBarang::join('transaksi','detail_transaksi.id_transaksi','=','transaksi.id_transaksi')
        ->where('transaksi.status','2')
            ->sum('detail_transaksi.jumlah');
        $stok = Barang::sum('stok');
        $transaksi = Transaksi2::where('status','2')->count();


        $hasil['total_penjualan'] = $total_penjualan;
        $hasil['barang_terjual'] = $barang_terjual;
        $hasil['stok'] = $stok;
        $hasil['transaksi'] = $transaksi;
        return view('home/index',$hasil);



    }
    public function about()
    {
        return view('about');
    }
}
