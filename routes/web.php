<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','Home@index');

Route::get('web', function (){
    return "A";
});
Route::get('/barang','Baru@index');
Route::get('/barang/tambah','Baru@tambah');
Route::post('/barang/store','Baru@store');
Route::post('/barang/update','Baru@update');
Route::get('/barang/export_pdf','Baru@export_pdf');
Route::get('/laporan/view','Baru@a');
Route::get('/login','Login@index');
Route::get('/barang/edit/{id}','Baru@edit');
Route::post('/barang/delete/{id}','Baru@delete');
Route::get('/transaksi','Transaksi@index');
Route::get('/transaksi/add','Transaksi@add');
Route::get('login', 'AuthController@index');
Route::post('post-login', 'AuthController@postLogin');
Route::get('registration', 'AuthController@registration');
Route::post('post-registration', 'AuthController@postRegistration');
Route::get('dashboard', 'AuthController@dashboard');
Route::get('logout', 'AuthController@logout');
Route::get('/barang/export','Baru@export');
Route::get('/gambar','Image@index');
Route::get('/gambar/add','Image@insert');
Route::post('/gambar/store','Image@store');
Route::get('/barang/search','Baru@search');
Route::post('/barang/import','Baru@import');
Route::get('/transaksi/add_barang/{id}','Transaksi@add_barang');
Route::post('/transaksi/beli_barang','Transaksi@beli_barang');
Route::post('/transaksi/bayar','Transaksi@bayar');
Route::get('/about','Home@about');
Route::get('/kirim','Baru@kirim');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
