<html>

<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/bootstrap/css/bootstrap-reboot.min.css" rel="stylesheet" type="text/css">
<link href="/bootstrap/css/bootstrap-grid.min.css" rel="stylesheet" type="text/css">

@yield('modal')
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Unnamed</a>
      </li>
      @guest
      @if (Route::has('register'))

      @endif
      @else
      <li class="nav-item">
        <a class="nav-link" href="/">Dashboard</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/barang">Barang</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/transaksi">Transaksi</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/gambar">Gambar</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/about">Tentang Aplikasi</a>
      </li>
      @endguest
    </ul>
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>
  </nav>
@yield('konten')

<script src="/jquery.js" type="text/javascript"></script>
<script src="/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<script src="/js/b.js" type="text/javascript"></script>
<script src="/js/transaksi.js" type="text/javascript"></script>

</html>
