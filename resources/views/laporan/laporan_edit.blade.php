<html>
<head>
    <title>Tambah Barang</title>
    </head>
    <body>
    <a href="/barang">Kembali</a>
    <h2>Tambah Barang</h2>
        
        <br>
        <br>
    @foreach($barang as $row)    
    <form action="/barang/update" method="post">
        {{ csrf_field() }}
        <label>ID Barang: </label>
        <input type="text" name="id" value="{{ $row->id }}" readonly>
        <br>
        <label>Nama Barang: </label>
        <input type="text" name="nama_barang" value="{{ $row->nama_barang }}">
        <br>
        <label>Harga Barang: </label>
        <input type="number" name="harga_barang" value="{{ $row->harga_barang }}">
        <br>
        
        <input type="submit" value="Tambah">
        </form>
        @endforeach
    </body>
</html>