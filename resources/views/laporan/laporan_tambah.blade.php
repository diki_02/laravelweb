<html>
<head>
    <title>Tambah Barang</title>
    </head>
    <body>
    <a href="/barang">Kembali</a>
    <h2>Tambah Barang</h2>
        
        <br>
        <br>
        
    <form action="/barang/store" method="post">
        {{ csrf_field() }}
        <label>Tanggal Laporan: </label>
        <input type="date" name="tgl_laporan">
        <br>
        <label>Tipe Laporan: </label>
        <select name="tp_laporan">
        <option value="Pengeluaran">Pengeluaran</option>
        <option value="Pemasukan">Pemasukan</option>
        </select>
        <br>
        <label>Deskripsi</label>
        <textarea name="deskripsi" maxlength="100"></textarea>
        <br>
        <label>Barang</label>
        
        <input type="submit" value="Tambah">
        </form>
    </body>
</html>