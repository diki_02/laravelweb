<html>
    @extends('master')
    @section('konten')
<head>
   <title>Tambah Barang</title>
    </head>
    <body>
    <h2>Tambah Barang</h2>
    <a href="/laporan/tambah">Tambah Barang</a>
       <br>
       <br>

        <table>
        <thead>
            <tr>
            <th>No</th>
            <th>Nama Barang</th>
            <th>Harga Barang</th>

            </tr>
            </thead>
            <tbody>
            @foreach($data as $row)
                <tr>
                <td>{{ $row->id }}</td>
                <td>{{ $row->nama_barang }}</td>
                <td>{{ $row->harga_barang }}</td>
                <td>
                    <a href="/barang/edit/{{ $row->id }}">Edit</a> |
                    <a href="/barang/delete/{{ $row->id }}">Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
    @endsection
</html>
