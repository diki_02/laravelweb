<!DOCTYPE html>
<html>
<head><title>Login</title></head>
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/bootstrap/css/bootstrap-reboot.min.css" rel="stylesheet" type="text/css">
<link href="/bootstrap/css/bootstrap-grid.min.css" rel="stylesheet" type="text/css">
<link href="/css/a.css" rel="stylesheet" type="text/css">
<body>

    <div class="container justify-content-center">
<div class="row">
    <div class="col-4" >
        <div class="c_">
        <div class="card color_ ">
        <div class="card-header bg-primary"><span class="login_">Login</span></div>
        <div class="card-body">
            <form action="#" method="post">

                    <div class=" col-12">
                        <div class="row">
                        <label>Username</label>
                        <input type="text" class="form-control">
                        </div>
                        <div class="row">
                        <label>Password</label>
                        <input type="password" class="form-control">
                        </div>
                        <br>
                        <div class="row">

                        <input type="submit" class="btn btn-primary b_">

                        </div>
                    </div>

            </form>

        </div>

      </div>
    </div>
    </div>

    </div>
</div>
</body>

<script src="/jquery.js" type="text/javascript"></script>
<script src="/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<html>
