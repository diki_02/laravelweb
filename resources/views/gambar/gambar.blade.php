<html>
    @extends('master')
    @section('konten')
    <head><title>
        Coba Upload Gambar Gambar
        </title>
    </head>
    <body>
        <div class="container">

            <div class="row">
                <a href="/gambar/add" class="btn btn-primary">Tambah Gambar</a>
            </div>
            <div class="row">
            <table class="table">
                <thead>
                    <th>Gambar</th>
                    <th>Deskripsi</th>
                </thead>
                <tbody>
                    @foreach($hasil as $row)
                    <tr>
                    <td><img src="/image/{{$row->filename}}" width="auto" height="100px"></td>
                    <td>{{ $row->keterangan }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </body>
    @endsection
</html>
