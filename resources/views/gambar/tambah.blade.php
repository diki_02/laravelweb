@extends('master')
@section('konten')

<html>
<head>
    <title>Gambar</title>
</head>
<body>
<div class="container">
    <div class="row">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
            </div>
            @endif
        </div>
    <div class="row">
        Tambah Gambar
    </div>
    <form action="/gambar/store" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
    <div class="row">
        <label>Gambar</label>
        <img id="preview" src="/image/gambar.jpg" width="320px" height="240px">
        <input type="file" class="form-control" name="gambar_" onchange="loadPreview(this)">
    </div>
    <div class="row">
    <label>Deskripsi </label>
        <textarea class="form-control" name="deskripsi"></textarea>
    </div>
    <div class="row">
        <button type="submit" class="btn btn-primary">Upload</button>
    </div>
</form>
</div>

</body>
<script>
function loadPreview(input, id) {
    id = id || '#preview';
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(id)
                    .attr('src', e.target.result)
                    .width(200)
                    .height(150);
        };

        reader.readAsDataURL(input.files[0]);
    }
 }
</script>
</html>

@endsection
