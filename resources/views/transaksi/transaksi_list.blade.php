<html>
@extends('master')
@section('konten')

<head>
<title> List Transaksi </title>


</head>
<body>
<br>
    <div class="container">
        <div class="row">
            <br>
            <div class="col col-5">
            <a href="/transaksi/add" class="btn btn-primary" id="add_transaksi">Lakukan Transaksi</a>
            </div>
            <br>
        </div>
        <br>
        <div class="row">
            <span class="text-center"><h1>List Transaksi</h1></span>
        </div>
        <br>
    <div class="row">
    <table class="table table-bordered table-stripped table-hover">
        <thead>
            <tr>
                <th class="text-center">ID Transaksi</th>
                <th class="text-center">Tanggal Transaksi</th>
                <th class="text-center">Status</th>
                <th class="text-right">Total Bayar</th>
                <th class="text-right">Jumlah Bayar</th>
                <th class="text-right">Kembalian</th>

            </tr>

        </thead>
        <tbody>
            @foreach($hasil as $row)
            @php
                $status = "";
            if($row->status == 1){
                $status = "Belum Selesai";
            } else if($row->status = 2){
                $status = "Selesai";
            } else {
                $status = "Error";
            } @endphp
            <tr>
            <td class="text-center">{{ $row->id_transaksi }}</td>
            <td class="text-center">{{ $row->tgl_transaksi }}</td>
            <td class="text-center">{{ $status }}</td>
            <td class="text-right">{{ number_format($row->total_bayar) }}</td>
            <td class="text-right">{{ number_format($row->jumlah_bayar) }}</td>
            <td class="text-right">{{ number_format($row->kembalian) }}</td>
            </tr>
            @endforeach
        </tbody>

    </table>
    {{ $hasil->links() }}
</div>
</div>
</body>

@endsection

</html>
