<html>

@extends('master')
@section('konten')
<br>
<br>
@php $barang_jumlah = number_format($barang_terjual);
$penjualan = number_format($total_penjualan);
@endphp
<div class="container">
    <div class="card-deck">
        <div class="card bg-primary text-white">
            <div class="card-body text-center">
                <div class="text-left"><small> Pendapatan</small></div>
                <div class="text-right"><h3> Rp.{{ $penjualan }}</h3></div>
              </div>
        </div>

        <div class="card bg-success text-white">
          <div class="card-body text-center">
            <div class="text-left"><small> Barang Terjual</small></div>
            <div class="text-right"><h3>{{ $barang_jumlah }}</h3></div>
          </div>
        </div>
        <div class="card bg-danger text-white">
            <div class="card-body text-center">
              <div class="text-left"><small> Stok Barang</small></div>
              <div class="text-right"><h3>{{ number_format($stok) }}</h3></div>
            </div>
          </div>
          <div class="card bg-warning text-white">
            <div class="card-body text-center">
              <div class="text-left"><small> Transaksi Dilakukan</small></div>
              <div class="text-right"><h3>{{ number_format($transaksi) }}</h3></div>
            </div>
          </div>

      </div>
      <br>
      <div class="card" style="">
        <div class="card-header">

        </div>
        <div class="card-body">
            <img src="/image/gambar.jpg" width="100%" height="auto">
        </div>

      </div>
</div>

@endsection
</html>
