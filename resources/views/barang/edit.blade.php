<html>
    @extends('master')
    @section('konten')
<head>
    <title>Tambah Barang</title>
    </head>
    <body>
    <div class="container">
    <a href="/barang" class="btn btn-primary">Kembali</a>
    <h2>Tambah Barang</h2>

        <br>
        <br>
    @foreach($barang as $row)
    <div class="row">
        <div class="col-8">
    <form action="/barang/update" method="post">
        {{ csrf_field() }}
        <label>ID Barang: </label>
        <input type="text" name="id" value="{{ $row->id_barang }}" readonly class="form-control">
        <br>
        <label>Nama Barang: </label>
        <input type="text" name="nama_barang" value="{{ $row->nama_barang }}" class="form-control">
        <br>
        <label>Harga Barang: </label>
        <input type="number" name="harga_barang" value="{{ $row->harga_barang }}" class="form-control">
        <br>
        <label>Stok: </label>
        <input type="number"  name="stok" class="form-control" value="{{ $row->stok }}" >
        <br>

        <input type="submit" value="Ubah" class="btn btn-primary">
        </form>
    </div>
    </div>
        @endforeach
    </div>
    </body>
    @endsection
</html>
