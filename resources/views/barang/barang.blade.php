<html>
    @extends('master')
    @section('konten')
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="modal fade" id="modal_impor" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Import Dari Excel </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                    <form action="/barang/import" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="file" class="form-control" name="file">
                        <br>
                        <input type="submit" class="btn btn-success" value="Import">

                    </form>
                </div>
                    </div>
            </div>

          </div>
        </div>
      </div>
    <div class="modal fade" id="modal_add" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Tambah Barang </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-8">
                    <form action="/barang/store" method="post" id="add_barang_form">
                        {{ csrf_field() }}
                        <label>Nama Barang: </label>
                        <input type="text" name="nama_barang" class="form-control" id="nama_barang2">
                        <br>
                        <label>Harga Barang: </label>
                        <input type="number" name="harga_barang" class="form-control" id="harga_barang2">
                        <br>

                        <input type="submit" value="Tambah" class="btn btn-success" id="btn_submit2">
                        </form>
                    </div>
                    </div>
            </div>

          </div>
        </div>
      </div>
      <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Ubah Detail Barang</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-8">
                <form action="/barang/update" method="post" id="edit_barang_form">
                    {{ csrf_field() }}
                    <label>ID Barang: </label>
                    <input type="text" name="id" id="id_barang" readonly class="form-control">
                    <br>
                    <label>Nama Barang: </label>
                    <input type="text" name="nama_barang" id="nama_barang" class="form-control">
                    <br>
                    <label>Harga Barang: </label>
                    <input type="number" name="harga_barang" id="harga_barang" class="form-control">
                    <br>

                    <input type="submit" value="Ubah" class="btn btn-primary" id="btn_submit">
                    </form>
                </div>
                </div>
            </div>

          </div>
        </div>
      </div>
   <title>Tambah Barang</title>
    </head>
    <body>
        <div class="container">
    <h2>Tambah Barang</h2>
    <div class="row">
        <div class="col">

    <a class="btn btn-success" href="/barang/tambah">Tambah Barang</a> <a class="btn btn-primary" href="/barang/export"> Export Ke Excel</a>
    <a href="/barang/export_pdf" class="btn btn-warning" target="blank">Export ke PDF</a> <button class="btn btn-danger" id="impor">Import Dari Excel</button>

</div>
    </div>
    <br>
    <br>
    <div class="row">
        <form action="/barang/search" method="GET">
            <div class="col">
                <label>Cari Barang</label>
                <input type="text" name="cari" class="form-control">
                <br>
                <input type="submit" class="btn btn-info" value="Cari">
            </div>

        </form>
    </div>
       <br>
       <br>
       <div class="row">
        <div class="col col-12">
        <table class="table table-stripped table-hover table-bordered">
        <thead>
            <tr>
            <th class="text-center">ID Barang</th>
            <th class="text-left">Nama Barang</th>
            <th class="text-right">Harga Barang</th>
            <th class="text-right">Stok</th>
            <th class="text-center">Action</th>

            </tr>
            </thead>
            <tbody>
            @foreach($data as $row)
                <tr>
                <td class="text-center">{{ $row->id_barang }}</td>
                <td class="text-left">{{ $row->nama_barang }}</td>
                <td class="text-right">{{ number_format($row->harga_barang) }}</td>
                <td class="text-right">{{ number_format($row->stok) }}</td>
                <td class="text-center">

                <a href="/barang/edit/{{ $row->id_barang }}" class="btn btn-info" >Ubah</a>
                    <a href="/barang/delete{{ $row->id_barang }}" class="btn btn-danger" >Hapus</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $data->links() }}
    </div>
    </div>
            </div>
    </body>

    @endsection
</html>
