<html>
    @extends('master')

    @section('konten')
<head>
    <title>Tambah Barang</title>
    </head>
    <body>
        <div class="container">
    <a href="/barang" class="btn btn-primary">Kembali</a>
    <h2>Tambah Barang</h2>

        <br>
        <br>
        <div class="row">
            <div class="col-8">
            <form action="/barang/store" method="post" id="add_barang_form">
                {{ csrf_field() }}
                <label>Nama Barang: </label>
                <input type="text" name="nama_barang" class="form-control" id="nama_barang2">
                <br>
                <label>Harga Barang: </label>
                <input type="number" name="harga_barang" class="form-control" id="harga_barang2">
                <br>
                <label>Stok: </label>
                <input type="number" name="stok" class="form-control" id="harga_barang2">
                <br>

                <input type="submit" value="Tambah" class="btn btn-success" id="btn_submit2">
                </form>
            </div>
            </div>
        </div>
    </body>
    @endsection
</html>
